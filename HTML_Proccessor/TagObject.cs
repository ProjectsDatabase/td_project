﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTML_Proccessor
{
    public class TagObject
    {
        private string _name;
        private int _appearances;
        private int _innerPostion;
        private string _attributes;
        private string _content;
        private List<TagObject> _containingTags;
        private bool _positonUpdated;

        public TagObject()
        {
            _containingTags = new List<TagObject>();
        }


        public TagObject(string name, int appearances)
        {
            _name = name;
            _appearances = appearances;
            _innerPostion = 0;
            _attributes = "";
            _content = "";
            _containingTags = new List<TagObject>();
        }

        public TagObject(string name, int appearances, int innerPostion, string attributes, string content)
        {
            _name = name;
            _appearances = appearances;
            _innerPostion = innerPostion;
            _attributes = attributes;
            _content = content;
            _containingTags = new List<TagObject>();
        }

        public string Name
        {
            set { _name = value; }
            get { return _name; }
        }

        public int Appearances
        {
            set { _appearances = value; }
            get { return _appearances; }

        }

        public int InnerPostion
        {
            set { _innerPostion = value; }
            get { return _innerPostion; }

        }

        public string Attributes
        {
            set { _attributes = value; }
            get { return _attributes; }

        }

        public string Content
        {
            set { _content = value; }
            get { return _content; }

        }


        public bool PositionUpdated
        {
            set { _positonUpdated = value; }
            get { return _positonUpdated; }

        }

        public List<TagObject> ContainingTags
        {
            set { _containingTags = value; }
            get { return _containingTags; }

        }
    }
}
