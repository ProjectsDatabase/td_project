﻿﻿﻿﻿﻿﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTML_Proccessor
{
    public class Program
    {
        static void Main(string[] args)
        {
            string text = System.IO.File.ReadAllText(@"C:\Users\Mariuca\Desktop\TD_HTML_Samples\Sample1.txt");

            // Display the file contents to the console. Variable text is a string.
            System.Console.WriteLine(text);

            System.Console.WriteLine("\n----------------------------------------------------------\n");
            
            SearchHelper searchHelper = new SearchHelper(text);

            string fileType = SearchHelper.getBetween(text, "<!DOCTYPE ", ">");

            if (string.Equals(fileType, "html", StringComparison.OrdinalIgnoreCase))
            {
                System.Console.WriteLine("The input is a HTML document");
            }
            else
            {
                System.Console.WriteLine("The input isn't a HTML document");
            }
            
            System.Console.WriteLine("\n----------------------------------------------------------\n");

            var tags = searchHelper.SearchHtmlTags();

            for (int i = 0; i < tags.Count(); i += 2)
            {
                //var current = SearchHelper.getBetween(tags[i].Name, "<", " ");
                var current = tags[i].Name.Substring(1);
                var next = SearchHelper.getBetween(tags[i + 1].Name, "</", ">");
                if (current == next && tags[i].Appearances == tags[i+1].Appearances)
                {
                    System.Console.WriteLine("The HTML tag " + current + " found " +
                                             (tags[i].Appearances + tags[i + 1].Appearances) + " times.");
                }
                else
                {
                    System.Console.WriteLine("Error HTML tag " + current + " found " +
                                             (tags[i].Appearances + tags[i + 1].Appearances) + " times.");
                }
            }

            System.Console.WriteLine("\n----------------------------------------------------------\n");

            var tagsWithPositions = searchHelper.GetTagsInnerPostion(tags);

            for (int i = 0; i < tagsWithPositions.Count(); i ++)
            {
                System.Console.WriteLine("HTML tag " + tagsWithPositions[i].Name + " on level " + tagsWithPositions[i].InnerPostion);
            }

            System.Console.WriteLine("\n----------------------------------------------------------\n");
            var parsedTags = searchHelper.Parse();
            var parsedTagsWithLevels = searchHelper.FindLevels(parsedTags);

            foreach (var parsedTag in parsedTagsWithLevels)
            {
                if (parsedTag.Attributes != null)
                {
                    System.Console.WriteLine("\n The tag \"" + parsedTag.Name + "\" with attributes [ " + parsedTag.Attributes + " ] found on level " + parsedTag.InnerPostion + " contains the following tags : ");
                }
                else
                {
                    System.Console.WriteLine("\n The tag \"" + parsedTag.Name + "\" found on level " + parsedTag.InnerPostion + " contains the following tags : ");
                }
                foreach (var containgTag in parsedTag.ContainingTags)
                {
                    System.Console.WriteLine("\n----> " + containgTag.Name);
                }
            }
            
            System.Console.ReadLine();
        }
    }
}
