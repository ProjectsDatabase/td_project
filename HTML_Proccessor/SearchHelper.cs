﻿﻿﻿﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HTML_Proccessor
{
    public class SearchHelper
    {
        private string input;

        private string openToken = "<";
        private string closeToken = ">";
        private string endToken = "</";

        public static List<string> normalTags = new List<string>()
        {
            "<html", "</html>",
            "<body", "</body>",
            "<frameset", "<frame", "<noframes",
            "</frameset>", "</frame>", "</noframe>",
            "<form", "<input", "<select", "<option",
            "</form>", "</input>", "</select>", "</option>",
            "<table", "<tr", "<td", "<th", "<thead", "<tbody",
            "</table>", "</tr>", "</td>", "</th>", "</thead>", "</tbody>",
            "<img", "</img>",
            "<a", "<link", "</a>", "</link>",
            "<ul", "<ol", "<li", "</ul>", "</ol>", "</li>",
            "<b>", "<i>", "<u>", "<small", "<sup", "<sub", "<center", "<font",
            "</b>", "</i>", "</u>", "</small>", "</sup>", "</sub>", "</center>", "</font>",
            "<h1", "<h2", "<h3", "<h4", "<h5", "<h6",
            "</h1>", "</h2>", "</h3>", "</h4>", "</h5>", "</h6>",
            "<p", "<hr", "<br",
            "</p>", "</hr>", "</br>",
        };

        public static List<string> noEndTags = new List<string>()
        {
            "<frame", "<input", "<img", "<link", "<hr", "<br",
        };


        public string Input
        {
            get { return this.input; }
        }


        public SearchHelper(string input)
        {
            this.input = input;
        }
        
        public List<TagObject> SearchHtmlTags()
        {
            List<TagObject> tagsFound = new List<TagObject>();
            string auxInput = input;

            foreach (string tag in normalTags)
            {
                while (auxInput.Contains(tag))
                {
                    if (tagsFound.Exists(t => t.Name == tag))
                    {
                        tagsFound.Find(t => t.Name == tag).Appearances++;
                    }
                    else
                    {
                        var newTag = new TagObject(tag, 1);
                        tagsFound.Add(newTag);
                    }
                    auxInput = auxInput.Remove(auxInput.IndexOf(tag), tag.Length);
                }
            }

            return tagsFound;
        }

        public List<TagObject> GetTagsInnerPostion(List<TagObject> tagsFound)
        {
            List<TagObject> tagsUpdated = new List<TagObject>();
            List<TagObject> tagsWithPositions = new List<TagObject>();


            foreach (var tag in tagsFound)
            {
                tag.InnerPostion = input.IndexOf(tag.Name);
                tagsUpdated.Add(tag);
                
            }

            for (int i = 0; i < tagsUpdated.Count; i += 2)
            {
                int position = 0;
                for (int j = 0; j< tagsUpdated.Count; j+=2)
                {
                    if (tagsUpdated[i].InnerPostion > tagsUpdated[j].InnerPostion &&
                        tagsUpdated[i].InnerPostion < tagsUpdated[j + 1].InnerPostion)
                    {
                        position++;
                    }
                }

                var auxTag = new TagObject(tagsUpdated[i].Name, tagsUpdated[i].Appearances);
                auxTag.InnerPostion = position;
                tagsWithPositions.Add(auxTag);
            }

            return tagsWithPositions;
        }

        public static string getBetween(string source, string strStart, string strEnd)
        {
            int Start, End;
            if (source.Contains(strStart) && source.Contains(strEnd))
            {
                Start = source.IndexOf(strStart, 0) + strStart.Length;
                End = source.IndexOf(strEnd, Start);
                return source.Substring(Start, End - Start);
            }
            else
            {
                return "";
            }
        }


        public List<TagObject> Parse()
        {
            string auxInput = input.Substring(15);
            string substring;
            string innerSubString;
            string tagname;
            int openTokenIndex;
            int closeTokenIndex;
            int level = 0;
            List <TagObject> parsedTags = new List<TagObject>();

           
            while (auxInput.Contains(openToken) == true)
            {
                TagObject tag = new TagObject();
                openTokenIndex = auxInput.IndexOf(openToken);
                substring = auxInput.Substring(openTokenIndex + 1);

                if (substring.IndexOf(closeToken) > substring.IndexOf(" "))
                {
                    closeTokenIndex = substring.IndexOf(closeToken);
                    tag.Name = substring.Substring(0, substring.IndexOf(" "));
                    tag.Attributes = substring.Substring(substring.IndexOf(" ") + 1, closeTokenIndex);
                }
                else
                {
                    closeTokenIndex = substring.IndexOf(closeToken);
                    tag.Name = substring.Substring(0, closeTokenIndex);
                }

                tag.InnerPostion = level;
                substring = substring.Substring(substring.IndexOf(closeToken));
                tagname = tag.Name;

                while (substring.IndexOf(openToken) < substring.IndexOf(endToken + tagname))
                {
                    TagObject innerTag = new TagObject();
                    openTokenIndex = substring.IndexOf(openToken);
                    innerSubString = substring.Substring(openTokenIndex + 1);
                    if (innerSubString.IndexOf(closeToken) > innerSubString.IndexOf(" "))
                    {
                        closeTokenIndex = innerSubString.IndexOf(closeToken);
                        innerTag.Name = innerSubString.Substring(0, innerSubString.IndexOf(" "));
                        innerTag.Attributes = innerSubString.Substring(innerSubString.IndexOf(" "), closeTokenIndex-1);
                    }
                    else
                    {
                        closeTokenIndex = innerSubString.IndexOf(closeToken);
                        innerTag.Name = innerSubString.Substring(0, closeTokenIndex);
                    }

                    innerTag.InnerPostion = level;

                    tag.ContainingTags.Add(innerTag);
                    substring = innerSubString.Substring(innerSubString.IndexOf(endToken + innerTag.Name) + innerTag.Name.Length + 3);
                }
                parsedTags.Add(tag);
                auxInput = auxInput.Substring(auxInput.IndexOf(closeToken)+1);
                auxInput = auxInput.Replace(endToken + tag.Name + closeToken, "");
            }

            return parsedTags;
        }

        public List<TagObject> FindLevels(List<TagObject> parsedTags)
        {
            int level = 0;

            var auxTags = parsedTags;
            parsedTags.First().InnerPostion = level;
            foreach (TagObject tag in parsedTags)
            {
                foreach (var containingTag in tag.ContainingTags)
                {
                    parsedTags.Find(t => t.Name == containingTag.Name && t.PositionUpdated == false).InnerPostion = tag.InnerPostion + 1;
                    parsedTags.Find(t => t.Name == containingTag.Name && t.PositionUpdated == false).PositionUpdated = true;
                }
            }


            return parsedTags;
        }

    }
}
